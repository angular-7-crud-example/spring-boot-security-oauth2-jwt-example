package com.hendisantika.springbootsecurityoauth2jwtexample.repository;

import com.hendisantika.springbootsecurityoauth2jwtexample.entity.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-oauth2-jwt-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/11/18
 * Time: 20.39
 */
public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);
}

