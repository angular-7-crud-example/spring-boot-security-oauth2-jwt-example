package com.hendisantika.springbootsecurityoauth2jwtexample.service;

import com.hendisantika.springbootsecurityoauth2jwtexample.entity.User;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-oauth2-jwt-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/11/18
 * Time: 20.40
 */
public interface UserService {
    User save(User user);

    List<User> findAll();

    User findOne(long id);

    void delete(long id);
}
